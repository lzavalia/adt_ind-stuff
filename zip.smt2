;tuple definition and functions
(declare-datatype () (Tup ((f Int) (s Bool))))

(declare-fun mkTup (Int Bool) Tup)
(assert(forall ((x Int)(y Bool)) (= (Tup ((f x)(s y)))(mkTup x y))))

(declare-fun fst (Tup) Int)
(assert (forall ((x Tup))(=(f x) x)))

(declare-fun snd (Tup) Bool)
(assert (forall ((x Tup))(=(s x) x)))

;List types and their functions
;the notiations is as follows
;   C := Cons
;   N := Nil
;   i := Int
;   b := Bool
;   t := Tup
;type polymorphism?
(declare-datatype () ((LstInt (Ci (hi Int) (ti Int))(Ni))))

(declare-fun leni (LstInt) Int)
(assert (= (leni Ni) 0))
(assert (forall ((x Int) (y LstInt)) (= (leni (Ci x y))(+ 1 (leni y)))))

(declare-fun appi (LstInt LstInt) LstInt)
(assert (forall ((x LstInt)) (= (appi Ni x) x)))
(assert (forall ((x Int) (y LstInt) (z LstInt) (= (appi (Ci x y) z) (Ci x (appi y z))))))

(declare-datatype () ((LstBool (Cb (hb Bool) (tb Bool))((Nb))))

(declare-fun lenb (LstBool) Int)
(assert (= (lenb Nb) 0))
(assert (forall ((x Int) (y LstBool)) (= (lenb (Cb x y))(+ 1 (lenb y)))))

(declare-fun appb (LstBool LstBool) LstBool)
(assert (forall ((x LstBool)) (= (appb Nb x) x)))
(assert (forall ((x Bool) (y LstBool) (z LstBool) (= (appb (Cb x y) z) (Cb x (appb y z))))))

(declare-datatype () ((LstTup (Ct (ht Tup) (tt Tup))((Nt)))))

(declare-fun lent (LstTup) Int)
(assert (= (lent Nt) 0))
(assert (forall ((x Tup) (y LstTup)) (= (lent (Ct x y))(+ 1 (lent y)))))

(declare-fun appt (LstTup LstTup) LstTup)
(assert (forall ((x LstTup)) (= (appt Nt x) x)))
(assert (forall ((x Tup) (y LstTup) (z LstTup)) (= (appt (Ct x y) z) (Ct x (appt y z)))))

;ZIP function from haskell
;corresponds to the following function
;zip :: [a] -> [b] -> [(a, b)]
;zip [] [] = []
;zip al bl = (\x y -> [(x, y)]) (head al) (head bl) ++ zip (tail al) (tail bl)
;with dependent types in Coq we can guarantee that [a] and [b] have the same length

(declare-fun zip (LstInt LstBool) LstTup)
(assert (forall ((x LstInt) (y LstBool)) (= (leni x) (lenb y))))
(assert (= (zip Ni Nb) Nt)
(assert (forall ((x LstInt y LstBool)) (=(zip (Ci (hi x)(ti x))(Cb (hb x)(tb y)))(Ct (ht (mkTup (hi x) (hb y)))(tt (mkTup (ti x) (tb y)))))))
